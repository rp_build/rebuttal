Fig xx shows the fitted results (lines) on the respective
measured points. The fitted curves have  a 97-98\% $R^2$ with experimental
data and p-values below $10^{-4}$. This indicates that although the flow
regime is not a fully developed dispersed flow at low flow rates (stratified
flow), the extraction kinetics still obey the relation with the energy
dissipation. This in turn allows for a guided scale-up of these mesoflow
reactors respecting the specific geometrical relations in the reactors, where
turbulence becomes more common. The specific interfacial area and mass
transfer constant for lower flow rates approximates that of the equivalent
droplets, but sometimes in the form of partially dispersed, partially
stratified in the experiments.

\begin{table}
\centering
\caption{The specific $A$ values for the relation between the energy
dissipation and the mass transfer rate.}
\begin{tabular}{l|ccc}
reactor & LTF-MX & vortex reactor & LTF-VS \\ \hline
A & 0.17 & 0.097 & 0.069 \\ \hline
\end{tabular}
\label{tab: A values for the mixersl}
\end{table}

In \ref{tab: A values for the mixersl} the resulting $A$ values of the fitting
are presented. Because the $A$-factor of the interfacial area ($a$) is mainly
dependent on the interfacial tension and the fluid density, which have been
matched with experimental data, the main difference must lie within the
$A$-factor of the mass transfer rate constant ($k_l$), which must incorporate
the reactor performance and geometrical influence.
