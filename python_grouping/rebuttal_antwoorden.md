# Ken B.

## Opmerkingen in boek

- Scope --> typfouten
- P15 --> 2 decades <-> 5-6 decennia
	+ several separation (steps toevoegen)
- P16 --> alle ", which" nakijken
- P25 --> zin aanvullen

> between energy dissipation [W/kg]

- P26 --> kristallisatie mogelijkheden toelichten (buiten degene beschreven)
- P29 --> typfout
- P31 --> typfouten
- P34 --> andere opties/problemen van segmented flow toelichten
- P48 --> uitleg, dan formule
- P63 --> typfouten
- P64 --> Ca nummer eerder toelichten (in inleiding)
- P66 --> typfouten
- P67 --> laatste zin vervolledigen

> They showed that by reducing the flow crossection on each step down to 2/3rd of the previous crossection

- P74 --> Onvolledige zin
- P84 --> typfout
- P90 --> letters bij caption, zelfde voor andere figuren
- P91 --> caption nakijken
	+	defocusing toelichten in tekst waarom
	+	plateaus toelichten (max capaciteit)
- P93 --> Kla naar kla omzetten
- P94 --> Kla naar kla
- P96 --> C6 waarde en constante toelichten en aangeven dat het empirische fit is
	+ typfouten
- P97 --> symbool uniformiteit
	+	resulting in eq. 4.8 <<<
- P109 --> combinatie tussen diffusie en reactie snelheid
	+	Vergelijkingen nakijken op juistheid (uitbreiden)
- P116 --> \Ref
	+	welke reactor bij figuur 5.4b
- P117 --> welke reactor bij figuur 5.5
- Publications --> typfout en onvolledig
- Short summary -->
	+	symbolen lijst
	+	referenties
	+	Ca number
	+	Plateau uitleggen
	+	eenheid C6 en variantie symbool
	+	diffusie versus reaction rate

## Opmerkingen gesprek

- Refs

> Al de referenties zijn nagekeken op volledigheid en format TODO

- Symbolen alfabetisch

> De symbolen zijn nu alfabetisch geordend en opgekuisd en uniform gemaakt naar volledigheid van de tekst TODO

- Capillary number: def geven en fysische betekenis

> Het capillaire getal is gedefinieerd op pagina met volgende paragraaf 

- Hoe kom je tot vgl. 4.8

> Afleiding verduidelijkt (C waarde in subscale en gedefinieerd als een range) TODO

------------------------------------------------------------------------

# Nico V.

## Opmerkingen in boek

- P15 --> wat is reden van decline
	+ --> microtas is geen doorbraak
- P16 --> voorbeelden van A,B,C
	+ --> voorbeeld gevaarlijke reactie
- P17 --> is het materiaal van bedrijven gestandardiseerd?
	+ De schaal 10-50 ml/min is nog steeds lage productie (toespitsten op organische synthese? of effectief kijken wat ze nog in aanbieding hebben)
- P19 --> definitie kolmogorov schaal
	+ --> typfout
- P22 --> concentration <-> solubility limitations
- P26 --> andere voorbeelden van kristallisatie processen
- P27 --> wat zijn zeolieten toelichten
- P62 --> verduidelijken wat verschil is met thesis en state of the art
- P63 --> claims toelichten met figuren (numerous attempts en type 2D en 3D distributor, slate design)
- P64 --> voorbeeld segmented flow, T-junction
	+ "one" found <-- wie is one
	+ lumb factor <-- anders verwoorden
	+ "at large" <-- vervangen
- P66 --> figuur toevoegen ter verduidelijking
	+ waarom niet zuiver H2O/oil (sds-micellen)
- P67 --> figuur van setup toevoegen
	+ several studies (slechts 2 gerefereerd)
- P68 --> fractal flow distributors herdefinieren
- P70 --> CFD details en settings weergeven in appendix
- P71 --> toelichten hoe flowrate gemeten is
- P72 --> DSD batch, statische menger toevoegen + berekening met ref
	+ mean diameter en span of span
	+ wat is resultaat van obfuscatie/lichtmeting op bepaalde hoogte ifv tijd
- P73 --> bewijs (zie video)
- P80 --> two accounts
- P84 --> waarom deze 3 types reactor
- P90 --> hoe zijn de punten gemeten (op zelfde volume ???)
- P92 --> vergelijking energie dissipatie/performantie in batch toevoegen
- P96 --> toelichten van c6 en berekeningen
- P120 --> conclusie SX toelichten naar industrie (waar toepasbaar)
- P130 --> hoe is de CCFR schaalbaar naar productie (van ul naar ml naar l) op die opstelling en wat zijn risicos


## Opmerkingen gesprek

- nergens definitie mesoflow reactor

> Definitie is toegevoegd in de introductie met volgende zin
>
> > Mesoflow reactors are continuous flow reactors with geometrical features with characteristic dimension between 1 mm and 1 cm.

- wat is reden afname publicaties?

> Uitleg toegevoegd onder vorm van volgende paragraaf:
>

- LTF, vortex, andere: waarom net die 3 reactoren?

> Deze 3 reactoren waren beschikbaar (net als de beschikbaarheid van een kolf voor organische chemisten)

- Pag. 16: welke reactie is bvb. exotherm? Ammoniak, gevaarlijke zaken: voorbeelden geven van verschillende types reacties

> voorbeelden geven in de introductie

- Kolmogorov scale? Wat? Geen antwoord.

> Kolmogorov schaal is de kleinste eddy die je kan vormen en bijgevolge druppelgrootte. Volgende zin is toegevoegd om dit te verduidelijken
>


- Pag 26: hoe kan je oversaturatie nog bereiken: koeling, anti-solvent kristallisatie: deze 2 types ook toevoegen

> De alternatieve vormen van kristallisatie zijn toegevoegd aan de paragraaf inclusief praktisch voorbeeld:
>

- H3: nood aan verduidelijkende Fign: Desmet design, bifurcating, etc.

> De vooropgestelde figuren zijn toegevoegd van verschillende distributoren om het verhaal te verduidelijken
>

- Pag. 64?

> ???
>

- Fig. 3.1 the main drawback… klopt dit, increase capillary number kna je niet halen uit Fig. 3.1

- P. 66: 2.5 D, 3D: Fig zou goed zijn

> De verschillen tussen 2.5 D en 3 D zijn toegelicht en een figuur is toegevoegd
>
> > paragraaf
>

- Waarom SDS toegevoegd? Truukje, maar ook voor batch gebruikt (ook bij Tom VG)

> De info van SDS is toegevoegd aan de desbetreffende paragraaf en vervolledigd met volgende tekst:
>
> > SDS was added to stabilize droplets both in the batch mixing and the flow reactor"
>

- Meer uitleg over simulaties: solvent settings (experimental hiervan)

> De solver settings en evaluaties met uitgebreidere informatie zijn toegevoegd in appendix alsook samengevat in een paragraaf:
>
> > paragraaf
>

- Fig. 3.7: hoe droplet size gemeten?

> De droplet size meting is verder uitgelicht in volgende paragraaf en toegevoegd:
>
- Soort PSD curve toevoegen (distributie)?pag. 72: ‘droplet distribution is large’: van waar? Uitleg?

> Droplet size distribution in batch reactoren is toegevoegd uit batch reactor met bijhorende PSD
>
- Pag. 73?
- P80: onduidelijk
- Fig. 4.5: op welke basis punten gekozen? Op basis van afstand? Eerlijk? Beter gelijke verblijfstijd? Nee. Vergelijking fair?
- Fig. 4.6 energie-input vergelijken met convent. Reactor: opzoeken in literatuur: grootte-ordes geven
- Selectiviteit beter of slechter met geteste reactoren? Hoe ver zit je van theor. max? Schalen op max?
- Fig. 4.9: kleine druppels ‘optisch’ weggefilter? Tobias: het is wel zo dat gebruikte theor. waarde op basis van
- Fig. 4.9 , betere keuze mogelijk dan 0.6
- Theor berekening 8 µm verschil met theorie: verhaal gaat niet op: 2 versch zaken? Waarom klopt theorie niet met exp?
- Alternatieve manier om watercontent te bepalen? Karl Fischer.

------------------------------------------------------------------------



# Tom VG

flow ratios --> flow rate ratios

## Notities van tekst

------------------



- Thesis outline --> chapter 4 titel toevoegen
- P12 --> Discussie van scaling en capaciteit toevoegen
- P14 --> Hoe verklaart je dit

> Although this endangers the process by heating the entire bulk mixture, which could lead to undesired side-products

- P15 --> fig 1.2 source aanpassen en wat is agloit
- P17 --> Typfouten in bedrijfsnamen
- P17 --> scaling strategien toelichten
- P17 --> eerder naar Roberge et al refereren en voorbeeld toevoegen van reactie types A, B en C
- P17 --> waar en wanneer is de capex study gedaan
- P17 --> zin vervolledigen


> This shows that the mvoement of microreactors from academic research to industrial applications

- P18 --> figuur strategie toelichten
- P19 --> Bedrijfsnamen opkuisen
- P19 --> typfouten corrigeren
- P20 --> rephrasen

> The microreactor can be thought of to be mixing in the microscale, which is confusing because it works in the mesoscale mixing level.

- P21 --> grafieken toelichten
- P22 --> typfouten
- P24 --> formule tussen grafiek en tekst eenduidig maken

> \(F = \frac{u_c G^\alpha a}{\sigma}\)

- P25 --> typfout
- P26 --> typfout
- P30 --> typfout
- P31 --> "The use of acoustic waves induces cavitations" &lt;-- niet altijd
- P32 --> 2de paragraaf opkuisen
	- \(t_m\) niet verwarren met mixing tijd voorgaand
	- Verduidelijk the shift van 4A naar SOD
- P44 --> en US (ultrasound)
- P45 --> CNC acroniem toelichten in tekst
	- typfout
	- the remaining techniques refereren naar tabel
- P47 --> verduidelijken dat er enkel met CNC chips is gewerkt en waarom laserablatie niet is gebruikt
	- Visualisatie van chip load (figuur)
- P48 --> referentie en typfout
- P49 --> dimensies toevoegen aan figuur (misschien combineren met chip load?)
- P50 --> tabel beduidende cijfers en hoofdletter voor Datron
- P52 --> 90/10 naar 90/10 %
- P53 --> figuur 2.5 gebruiken in tekst.
- P56 --> waarom CNC in chapter 3-5 en laserablatie in 6
- P63 --> rephrase

> As a third, these are the bifurcating flow distributors, which were studied are derived from nature

- P65 --> Rephrase

> A similar effect is at large when using flow focusing devices.

- P66 --> typfouten
- P67 --> rephrasen en typfouten

> They showed that by reducing the flow crossection on each step down the 2/3rd of the previous crossection.

- P68 --> ontbrekend werkwoord
- P70 --> 3 of 3.6 % opzoeken in data
- P71 --> flow richting en kleurschaal toevoegen
	- Below a flow ratio of 0.1, an increase in droplet size and span is noticed
		- Dit opvangen door statistische studie op data en toevoegen
- P72 --> foutbar data toevoegen
	- Wat is net proportioneel met d^2
	- Referentie toevoegen aan it is known
- P73 Figuren vervangen door link naar filmpjes voor vergelijk
	- it is clear --> tis niet duidelijk (op te vangen door quantitatieve data van scheiding? of referentie naar filmpjes toevoegen
	- typfout
- P74 --> dit valt niet uit de tekst te halen of is niet besproken

> allowing for a droplet generation at 20-30 ml/min with a droplet size coefficient of variance less than 9 %, whereas less

- P79 --> Chemical Engineering Journal
- P80 --> two accounts ??
- P81 --> result(s)
- P90 --> niet volledig duidelijk, is het mogelijk om een voorbeeld te geven van performantie verschillen
- P91 --> consistentie van grafiek assen
- P92 --> figuur 4.6b toelichten van flow profiel van de ltf-mx
- P93 --> wat zou effectn zijn van andere flowrate ratios of allesinds toelichten in grafiek
	- is er effectief een groot verschil tussen 2.5 en 6 seconden verblijftijd
- P94 --> tpfout
- P96 --> C6 definieren als range constante
	- theoretisch maximum en effectieve diameter toelichten, referentie naaar eq 4.8 en condities toevoegen van test
- P97 --> grafiek omzetten naar diameter ipv straal
- P98 --> sensitivity analysis of the emperical fit??
	- is er een statistisch significant verschil
	- De studie is gekanteld naar energie dissipatie maar wat met settling (kan daar de zelfde veronderstelling gemaakt worden)
- P107 --> Purification Technology
- P111 --> gl -> g/l en Cyanex
- P113 --> selectiviteit een eigen vergelijking
- P115 --> error bars? Definitie van E en selectiviteit toevoegen
- P116 --> hoeveel repetities
- P116 --> De selectiviteit van 23 op P117 slaat op?
	- De grafiek bestaat enkel uit de vortex mixer (spheres op figuur 5.4)
- P127 --> typfout
- P134 --> discussie tussen kristallisatie snelheid en reactie snelheid toevoegen in paragraaf (of highlighten)
- P135 --> temperatuur verhogen zorgt mss voor tragere groei versusr eactie met gevolg dat het mss beter is?
	- opzoeken
- P153 --> Zeer algemene conclusie
	- Meer detailleren wat impact op SX nu juist is
	- Wat is impact op specifieke kristallisatie
	- Welke kennisgaten moeten er nog effectief worden dichtgereden voor industriele toepassingen mogelijk zijn



------------------------------------------------------------------------

Opmerkingen tijdens defense

---------------------------

- Slordige tekst, refs drama, statements te gratuit (zonder ref., staving, heel moeilijk om inhoud te begrijpen)

> - De tekst is grondig nagelezen op volledigheid van zinnen
> - De referenties zijn vervolledigd
> - TODO --> statements, hoe dit opvangen?

- Er ontbreken veel technische details: spreiding, procescondities, definities: kan thesis verbeteren

> De tekst is nagelezen en de voorgestelde informatie is toegevoegd aan de tekst en verduidelijkt de setting TODO

- Chemische reactoren in 3 verschillende types

> De opsplitsing ABC en overige is verduidelijkt en telkens toegelicht met een beschrijvend voorbeeld:
> De 3 voorbeelden in zin vorm - - -

- Fig. 3.7 geen verschil in gem. tussen 2 punten wegens grote foutenvlaggen? Variaties in totale flow rate: meer info nodig. Die resultaten moeten meer uitwerken

> De grafieken zijn herbekeken en statistisch geanalyseerd (STD, span, mean, r2-fit, p-waarde) waarnaar de conclusie is bijgeschaafd:
>  De conclusie en de nuttige waarden

- Link naar youtube/website Fig. 3.8?

> De beelden zijn geupload naar een video service en een directe referntie is toegevoegd naar het beeldmateriaal
>

- Vgl sel (pag. 113): apart nummer geven

> De selectiviteit is toegevoegd als aparte vergelijking
>

- CNC/ablatie: waarom afwisselend ablatie CNC, etc.: staat er fout in: allemaal met CNC

> Het hoofdstuk is aangepast en verduidelijkt dat CNC de tool is geweest om de gebruikte reactoren te fabriceren
>
- Fig. 4.6: waarom gaat xx curve: , 4.6 B niet duidelijk

- Fig. 4.7: hoe significant zijn verschillen? Noticable shift between 2.5 s vs. 6 s? Toelichten

> Hier moet ik uitleg geven naar dat verschil en refereren naar voorgaande studies en waarom die shift er is
>

- Rare manier resultaten bespreken. Eerst zeggen: dit is wat ik zie, erna ev. verwijzen naar theorie (en niet omgekeerd).

> De bespreking voor de figuren is waar mogelijk aangepast van theorie --> observatie naar observatie --> theorie
>

- Bije elke Fig gebruikte symbolen opnieuw definiëren

> De captions van alle figuren zijn aangepast en vervolledigd met beschrijvende informatie en symbolen
>

- Uniformiseren: diameter versus r

> De tekst is aangepast naar straal (r)
>

- Fig. 6.5: waarom enkel bij hoge T kistallen? Verklaring? Discussie ook toespitsen op kristallisatie. Effect grootte? Kristalliniteit?

> De verklaring en discussie is bijgeschaafd met het onderscheid tussen kristallisatie en reactie
>
> > Paragraaf
>

- Onderscheid maken tussen kristallisatie en reactie. Reden ook dat je bij hogere T kristallisatie trager. Zoeken in literatuur.

> zie voorgaande argumentatie >

------------------------------------------------------------------------

# Tom B

algemene opmerking --> taalfouten

## Opmerkingen tekst

- Scope --> typfouten oplossen
- P13 --> are we working on the first or third pillar in this work, or a combination of both?
	+	baffle or blade ??
	+	which <>
- P14 --> figuur 1.1 E = fout (spreiding)
- P15 --> explain decrease in citations/research

- P18 --> some sacrifices are made during scale-up, which ones?
	+	Example?
	+	does it all depend on the most critical ones? Explain
- P19 --> what is bifurcation?
	+	Kolmogorov scale? dieper uitleggen
- P21 --> fig 1.5 it is not clear for me what is in the Y-axis, also in the caption (a), (b)
- P25 --> Correlatie We met $\varepsilon$
	+	power number toevoegen
- P25 --> Kolmogorov scale?
- P44 --> Did you consider 3D printing? Why not, what are the drawbacks (list it)
- P45 --> waarom dan wel lithografie in tekst?
- P46 --> fig 2.1 explain the evaluation of laser ablation and CNC milling (clarify chapter to distinguish)
- P47 --> discrepantie in tekst --> uniform trekken en duidelijk maken (zie vorige comment)
- P50 --> wat is accuracy van frezen?
- P49 --> What about accuracy? (this is in respect to laser ablation --> the reason it is omitted)
- P52 --> new tool (vervangen door fresh tool)
	+	DCM methode beter toelichten (met illustratie?)
- P53 --> with some smart design ...
- P55 --> skilled operator ...
	+	through cuts <> partial cuts ??
- P63 --> what are the pitfalls of numbering up?
	+	Is flow distribution easy?
	+	Lange zinnen
- P64 --> illustratie toevoegen van 2D en 3D distributoren
	+	wie is "one"
	+	lumb factor?
	+	Ca nummer toelichten
- P65 --> fig 3.1 what is the slope 1:1 (clarify)
	+	What is the theoretical idea behind this (dripping -> squeezing -> jetting?)
- P68 --> figuur 3.3 toelichten dat het duidelijk is dat uitgangen uitgangen zijn
	+ te lange zin
- P69 --> fig 3.4 beter toelichten
- P71 --> describe fig 3.7(b), equal total flow rate <-> fig 3.7(b).
	+	Increase in droplet size (not apparent, statistical analysis)
- P70 --> fig 3.5 variance 3 <-> 3.6 %
- P72 --> fig 3.8 phase separation not clear (movie)
- P87 --> describe the 4 zones (enhance figures and measuring zones)
- P90 --> why pH graph and high fluorescence @ 0.85 W/O ratio
	+	non-linear behavior continues below 10 ml/min
- P92 --> verklaar fig 4.6 nader
- P93 --> verklaar waarom LTF-MX minder is op lage flowrate (stratified)
- P94 --> fig 4.7 noticeable shift from 2.5 to 6 seconds (explain)
- P97 --> N_sc toelichten
	+	radius < > diameter
- P99 --> eq 4.8 explain c6
- P109 --> wat is ondergrens van druppelgrootte waar ze nog nuttig zijn (kolmogorov scale helps)
- P116 --> fig 5.4b differentiate between LTF-MX and vortex mixer (this is vortex mixer)
- P119 --> fig 5.7 in log scale?
- P129 --> what is NaA and what is a linde type A topology (inform)
	+	how to deal with RTD in CSTR
- P134 --> what is difference between stirred and unstirred batch heating (include information)
- P135 --> tolerance barrier in fig 6.4(a) but not in (b) (include it)
- P136 --> you mean the comparison with a batch reactor AFTER 2 hours (yes, need to reach temp)
- P137 --> you mention no crystalline material in the CFR for 16 min at 100 C, explain this)
- Pxx --> fig 6.9 is not clear (caption and legend)
- Pxx --> hoe betrouwbaar is reproductie data, vooral duidend op hfdst. 5
- Pxx --> numbering up in future perspectives?

## Opmerkingen discussie

- Procesintensificatie. In welke pijler? Toelichten! Combinatie 1 en 3

> De pijlers zijn toegelicht, telkens met een sprekend voorbeeld en een paragraaf is toegevoegd om het werk te situeren
>
> > paragraaf/zin
>
> > Paragraaf/zin 2
>

- Fig. 1.1 Wat is E?

- Fig. 1.5 y as legende onduidelijk

- Pag. 25: laatste zin: vanwaar komt dit? Power number?

- Limitaties numbering up?

> De limitaties van numbering up of de kostfactor is toegevoegd aan het werk
>

- Pag 70 3.6 % en 3% (Fig. 3.6)

- Fig. 4.1 andere observation points: zal te zien zijn bij kleur versie

> De figuren zijn aangepast en verduidelijkt zodanig dat het op zwart-wit print ook duidelijk is
>

- Pag. 109: the only wa y is by decreasing the size: optimum? Kort door de bocht statement

> De statement is uitgebreid met volgende tekst:
>

- Pag. 129 Zeoliet Linde type A topologie? Zijn er ook andere topologieën?

> Ja er zijn zeker andere topologien IZA( international zeolite association - KUL) heeft hier een duidelijke beschrijving van (zie ref)
>
> > ref:referentie naar iza
>
> De tekst is verduidelijkt en de link is ingevoegd om de topologie te plaatsen
>

- Fig. 4.4 wat is stippellijn?
- Fig. 4.4 Vanaf 10 ml niet lineair. Eronder wel? Statement aanpassen: is gewoon normaal gedrag.

> De statement is aangepast naar volgende paragraaf:
>

- Afwijkingen door milling?

> De afwijking van milling is 3-4 µm in xy-richting op de instelling
>
> > Deze afwijking bij goede instellingen is reproduceerbaar en heeft weinig impact op de totale flow karakteristieken aangezien de kanaal dimensies typisch > 1 mm zijn.
> >

- Mogelijke fabricagetechnieken. Zou je ook andere technieken gebruiken als geld geen issue zou zijn.
- Fig. 6.4b: tolerance boundary: woorom niet bij Fig. 6.4 a

> De tolerantie boundaries zijn ook toegevoegd aan fig 6.4a

- Fig. 6.9: waarom afwijkend punt: uitleggen, driehoek toevoegen zou nuttig zijn

------------------------------

# Johan DC

## Opmerkingen gesprek

- Druppelproductie: Vergelijk met conventionele methode: E nodig per debiet
- Wat is onderliggende reden 4 pillaren? Entropie? Alles wat niet gelijk is en niet even snel reageert vermindert de efficiëntie.
- Hoe ziet u verdere valorisatie?
	- Voor umicore: partikels
	- Farma: druppels, alleen aan grote, of ook in grote aantallen, hoe goedkoop te maken, lijn doortrekken naar praktijk
- Aspect praktisch vervolg: uit de doeken doen in thesis
	- Valorisatie
	- Kost: investering voor bedrijf
- Belangrijke conclusies duidelijk geven in tekst, ook wat over valorisatie
- Wijzigingen in track changes

-----------------------------

# Heidi O

- Welke image analysis? Nauwkeurigheid? Afschatting fout? Contrast te verbeteren bij Fig. 4.9
- Waarom voor PMMA gekozen?

-----------------------------------

# Kris D

- Trend academic output? Specifiek (gebrek aan literatuur bij) hydrometallurgie toelichten
- Fig. 4.9: waarom bimodaal?
- Wat voor mixers gebruiken bij slurries? Mixers daar ook voor geschikt?
