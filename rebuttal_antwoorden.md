# sidenotes

- energie in batch reactor: $\varepsilon = \frac{N_p N^3 d^5}{V}$
	+ N_p = power number (.37 voor turbulente marine schroef impeller)
	+ N = RPM
	+ d = schroef diameter
	+ V = volume 

# Ken B.

## Opmerkingen in boek

- Scope --> typfouten
	+ > aangepast
- P15 --> 2 decades <-> 5-6 decennia
	+ > 2 decennia, 5-6 jaar
	+ several separation (steps toevoegen)
		* > added
- P16 --> alle ", which" nakijken
	+ Gecontroleerd
- P25 --> zin aanvullen
	+ > done
- P26 --> kristallisatie mogelijkheden toelichten (buiten degene beschreven)
	+ > aangepast, zie comments Nico V
- P29 --> typfout
	+ > aangepast
- P31 --> typfouten
	+ > aangepast
- P34 --> andere opties/problemen van segmented flow toelichten
	+ > aangepast
- P48 --> uitleg, dan formule
	+ > aangepast
- P63 --> typfouten
	+ > aangepast
- P64 --> Ca nummer eerder toelichten (in inleiding)
	+ > eerder in inleiding toegevoegd
- P66 --> typfouten
	+ > aangepast
- P67 --> laatste zin vervolledigen
	+ > aangepast
- P74 --> Onvolledige zin
	+ > aangepast
- P84 --> typfout
	+ > aangepast
- P90 --> letters bij caption, zelfde voor andere figuren
	+ > aangepast
- P91 --> caption nakijken
	+	defocusing toelichten in tekst waarom
		*	> defocusing door diepte verwijdert uit tekst om doel van paragraaf te verduidelijken
	+	plateaus toelichten (max capaciteit)
		*	> geen effectieve plateau, zin verwijdert
- P93 --> Kla naar kla omzetten
	+ > done
- P94 --> Kla naar kla
	+ > done
- P96 --> C6 waarde en constante toelichten en aangeven dat het empirische fit is
	+ > aangepast zie comments Nico
	+ typfouten
		* > aangepast
- P97 --> symbool uniformiteit
	+	resulting in eq. 4.8 <<<
		*	> aangepast
- P109 --> combinatie tussen diffusie en reactie snelheid
	+	> aangepast om te verduidelijken
		*	> The rate at which the droplets migrate due to density differences is expressed as the terminal velocity: 
			> $v_t = \sqrt{4g d (\Delta \rho) (3 \rho_{cont}C_{drag})^{-1}}$
			> In case of low Reynolds numbers (less than 2) the drag coefficient ($C_{drag}$}
			> scales linear with the Re number resulting in:
			> $v_t = \frac{1000 g d^2(\rho_h - \rho_l)}{18 \mu_l}$
			> While the surface-to-volume ratio is proportional to $d^{-1}$.
- P116 --> \Ref
	+	welke reactor bij figuur 5.4b
		*	> aangepast
- P117 --> welke reactor bij figuur 5.5
	+ > aangepast
- Publications --> typfout en onvolledig
	+ > aangepast
- Short summary -->
	+	symbolen lijst
		*	> aangepast
	+	referenties
		*	> gecontroleerd
	+	Ca number
		*	> eerder beschreven
	+	Plateau uitleggen
		*	> Verwijdert om verwarring en onduidelijkheid te vermijden
	+	eenheid C6 en variantie symbool
		*	> tekst aangepast
	+	diffusie versus reaction rate
		*	> tekst aangepast en volgende argumentatie is toegevoegd
		* > het Damköhler getal (II) geeft antwoord op deze vraag:
		* > $Da_{II} = \frac{kC_0^{n-1}}{k_g a}$
		* > Met k de reactie snelheid, n de reactie orde, $k_g$ de globale massa transfer en a het specifiek oppervlak
		* > by een massa transfer $k_g= 1e-9$ en een reactie snelheid $k=1e-3$ is $Da_{(II)}=1.0$ op een diameter van 6 µm.
		![Damköhler curve](C:/Users\tabias/OneDrive/PHDTHESIS/Rebuttal/Berekeningen/Damkohler.png)

## Opmerkingen gesprek

- Refs
	+ > Al de referenties zijn nagekeken op volledigheid en format
- Symbolen alfabetisch
	+ > De symbolen zijn nu alfabetisch geordend en opgekuisd en uniform gemaakt naar volledigheid van de tekst 
- Capillary number: def geven en fysische betekenis
	+ > Het capillaire getal is gedefinieerd in de introductie en nogmaals in de introductie van het desbetreffende hoofdstuk
- Hoe kom je tot vgl. 4.8
	+ > Afleiding verduidelijkt (C waarde in subscale en gedefinieerd als een range) 

------------------------------------------------------------------------

# Nico V.

## Opmerkingen in boek

- P15 --> wat is reden van decline
	+ --> microtas is geen doorbraak
 		* > Industriele aanvaarding voor de technieken is beperkt
		* > Een grote diversiteit aan aanbieders/fabrikanten maakt eenduidigheid voor experimenteel werk zeer moeilijk zonder voorkennis.
 		* > Academische studies leunen als maar meer op het analytische aspect en gebruiken flow reactoren meer als een analyse tool voor opzuivering/opconcentrering dan syntheses of producties
		* > De volgende paragraaf is toegevoegd
			* > The decline in academic output can be attributed to a lack of acceptance in industrial applications. This could be either by the diversity of flow reactor types without standardization (such as a conventional flask) or the benefits do not outweigh the costs and risks of a flow reactor setup for the desired cases.

- P16 --> voorbeelden van A,B,C
	+ --> voorbeeld gevaarlijke reactie
		* > type A: two steps organometallic reaction from goberge
		* > type B: zeolite synthesis
		* > type C: formation of ammoniak by the haber-bosch method
- P17 --> is het materiaal van bedrijven gestandardiseerd?
	+ De schaal 10-50 ml/min is nog steeds lage productie (toespitsten op organische synthese? of effectief kijken wat ze nog in aanbieding hebben)
		* > De scaling van 10-50 ml/min is in de 2de generatie, de opvolgende generaties geven capaciteiten van 100-600 ml/min. Dit is nog steeds beneden generieke hydrometallurgische processen maar is wel al bruikbaar voor kleinere productie schalen.
- P19 --> definitie kolmogorov schaal
		* $\eta = \nu^{3/4} \varepsilon^{-1/4}$ is the Kolmogorov length scale
induced by turbulent movement and is assumed the smallest possible length scale
achievable.
	+ --> typfout
		* gecorrigeerd
- P22 --> concentration <-> solubility limitations
	+ gecorrigeerd
- P26 --> andere voorbeelden van kristallisatie processen
	+ Andere voorbeelden zijn toegevoegd en de paragraaf is aangepast naar
	+ > A process of which the final product after a phase change is a crystalline material is called a crystallization. The crystallization from liquid is triggered by oversaturation \cite{Mersmann2001} either obtained by reduction in solubility. This can be achieved under the form of cooling-crystallization (molasses crystallization) or by solvent evaporation (sucrose crystallization). An alternative is to perform a reactive crystallization. This increase is a consequence of a chemical reaction synthesizing very low soluble crystals. The rate at which precipitation proceeds, often defines the complexity of the reaction. Both to slow or too fast synthesis has it is own drawbacks. 
- P31 --> wat zijn zeolieten toelichten
	+ zin omgezet naar crystal synthesis ipv zeoliet
- P62 --> verduidelijken wat verschil is met thesis en state of the art
	+ aangepast
- P63 --> claims toelichten met figuren (numerous attempts en type 2D en 3D distributor, slate design)
	+ done
- P64 --> voorbeeld segmented flow, T-junction
	+ "one" found <-- wie is one
		* > Garstecki 2006 et al.
	+ lumb factor <-- anders verwoorden
		* > verwijderd
	+ "at large" <-- vervangen
		* > is present
- P66 --> figuur toevoegen ter verduidelijking
	+ toegevoegd
	+ waarom niet zuiver H2O/oil (sds-micellen)
		* > druppel stabilisatie
- P67 --> figuur van setup toevoegen
	+ > Figuur flowcell
	+ several studies (slechts 2 gerefereerd)
	+ aangepast
- P68 --> fractal flow distributors herdefinieren
	+ > By intertwining two fractal flow distributors and connecting the outlets in a T-junction manner, it is possible to control the conditions at each T-zone in a precise manner.
- P70 --> CFD details en settings weergeven in appendix
	+ appendix toegevoegd van de cfd
- P71 --> toelichten hoe flowrate gemeten is
	+ > The flow rates of the individual streams were measured (by means of g/min) prior to the experiments and correlated to the vesse
- P72 --> DSD batch, statische menger toevoegen + berekening met ref
	+ mean diameter en span of span
		* > tekst aangepast en DSD toegevoegd
	+ wat is resultaat van obfuscatie/lichtmeting op bepaalde hoogte ifv tijd
- P73 --> bewijs (zie video)
	+ video geupload en tekst aangepast
- P80 --> two accounts
	+ > duidend op a(specific area) en $k_la$
- P84 --> waarom deze 3 types reactor
	+ > Beschikbaarheid en fabricage mogelijkheid.
		* > Dit toont nogmaals het probleem van diversificatie in mesoflow reactoren aan.
- P90 --> hoe zijn de punten gemeten (op zelfde volume ???)
	+ > De metingen zijn gedaan op praktische intervallen. Elk met hun respectievelijk volume. Vandaar de curve ifv upstream reactor volume.
	+ > De correlatie ligt hem in het vergelijk per volume eenheid.
- P92 --> vergelijking energie dissipatie/performantie in batch toevoegen
	+ > toegevoegd (Po)
- P96 --> toelichten van c6 en berekeningen
	+ > $c_6$ is een emperische fit.
	+ > De berekeningen zijn nagekeken met de originele data en de tekst is naderhand aangepast.
	+ > By image analysis, an average droplet radius of 40 $\mu m$ was found with a maximum of 52 $\mu m$, measured on 146 droplets over several images. This maximum radius deviates 8 $\mu m$ from the theoretical maximal radius of 44-49 $\mu m$ utilizing a $c_6$ value of \"0.53-0.6\".

- P115 --> Selectiviteit toelichten en evenwichtslijn toevoegen.
	+ > aangepast
- P120 --> conclusie SX toelichten naar industrie (waar toepasbaar)
	+ > toegelicht
		* > The useful time (ratio between mixer-settler equilibrating/total time) is 1 leading to a total flow through time of 15 min. Therefore the optimal parameters could be screened in a rapid pace on a bench top. In comparison to classical mixer-settler setups in industry, which take several hours to equilibrate over multiple stages. The flow reactor setup has a smaller internal volume, requiring less solvent/extractant for the screening process compared to mixer-settler lab setups.
		* 
- P130 --> hoe is de CCFR schaalbaar naar productie (van ul naar ml naar l) op die opstelling en wat zijn risicos
	+ > Door parallellisatie en in acht nemende dat de header voor vloeistofverdeling geen kritische factor is. Is het mogelijk om dit te schalen. Het grote nadeel is het gebruik van segmented flow, die vervolgens nog eens moet afgescheiden worden.


## Opmerkingen gesprek

- nergens definitie mesoflow reactor
	+ > Definitie is toegevoegd in de introductie met volgende zin
		* > Mesoflow reactors are continuous flow reactors with geometrical features with characteristic dimension between 1 mm and 1 cm.
- wat is reden afname publicaties?
	+ > mogelijke interpretatie toegevoegd
- LTF, vortex, andere: waarom net die 3 reactoren?
	+ > Deze 3 reactoren waren beschikbaar (net als de beschikbaarheid van een kolf voor organische chemisten)
- Pag. 16: welke reactie is bvb. exotherm? Ammoniak, gevaarlijke zaken: voorbeelden geven van verschillende types reacties
	+	> voorbeelden geven in de introductie
- Kolmogorov scale? Wat? Geen antwoord.
	+> Kolmogorov schaal is de kleinste eddy die je kan vormen en bijgevolge druppelgrootte.
- Pag 26: hoe kan je oversaturatie nog bereiken: koeling, anti-solvent kristallisatie: deze 2 types ook toevoegen
	+ > De alternatieve vormen van kristallisatie zijn toegevoegd aan de paragraaf inclusief praktisch voorbeeld:
- H3: nood aan verduidelijkende Fign: Desmet design, bifurcating, etc.
	+ > done
> De vooropgestelde figuren zijn toegevoegd van verschillende distributoren om het verhaal te verduidelijken
- Pag. 64?
	+ > aangepast
- Fig. 3.1 the main drawback… klopt dit, increase capillary number kna je niet halen uit Fig. 3.1
	+ > aangepast
- P. 66: 2.5 D, 3D: Fig zou goed zijn
	+ > De verschillen tussen 2.5 D en 3 D zijn toegelicht en een figuur is toegevoegd
- Waarom SDS toegevoegd? Truukje, maar ook voor batch gebruikt (ook bij Tom VG)
	+ > De info van SDS is toegevoegd aan de desbetreffende paragraaf en vervolledigd met volgende tekst:
- Meer uitleg over simulaties: solvent settings (experimental hiervan)
	+ > De solver settings en evaluaties met uitgebreidere informatie zijn toegevoegd in appendix:
		* > appendix 1 toegevoegd

- Fig. 3.7: hoe droplet size gemeten?
	+ > optisch gemeten
	+ > De droplet size meting is verder uitgelicht in volgende paragraaf en toegevoegd:
- Soort PSD curve toevoegen (distributie)?pag. 72: ‘droplet distribution is large’: van waar? Uitleg?
- > Droplet size distribution in batch reactoren is toegevoegd uit batch reactor met bijhorende PSD
- Pag. 73?
- P80: onduidelijk
- Fig. 4.5: op welke basis punten gekozen? Op basis van afstand? Eerlijk? Beter gelijke verblijfstijd? Nee. Vergelijking fair?
- Fig. 4.6 energie-input vergelijken met convent. Reactor: opzoeken in literatuur: grootte-ordes geven
- Selectiviteit beter of slechter met geteste reactoren? Hoe ver zit je van theor. max? Schalen op max?
- Fig. 4.9: kleine druppels ‘optisch’ weggefilter? Tobias: het is wel zo dat gebruikte theor. waarde op basis van
- Fig. 4.9 , betere keuze mogelijk dan 0.6
- Theor berekening 8 µm verschil met theorie: verhaal gaat niet op: 2 versch zaken? Waarom klopt theorie niet met exp?
- Alternatieve manier om watercontent te bepalen? Karl Fischer.

------------------------------------------------------------------------



# Tom VG

flow ratios --> flow rate ratios

## Notities van tekst

------------------

- Thesis outline --> chapter 4 titel toevoegen
	+ > outline aangepast
- P12 --> Discussie van scaling en capaciteit toevoegen
	+ > toegevoegd
- P14 --> Hoe verklaart je dit
	+ > Although this endangers the process by heating the entire bulk mixture, which could lead to undesired side-products
		* > verkeerd woordgebruik en locatie in tekst. De tekst is aangepast.
		* > It is aimed towards the microscopic scale of \keyword{PI} utilizing the capability of high surface-to- volume ratios of micro- and mesoflow reactors with specific geometries. In conventional reactors the heat transfer rate is often slow, leading to large variations in temperature in the reactor itself. It is clear that this can be improved by increasing the homogeneity of temperature in the entire liquid at the same time. The main limitation is ratio between the liquid volume/mass of the reactor and the available heat-transfer area. When utilizing to mesoflow reactors, the enhanced surface-to-volume ratio and heat transfer can be exploited in reverse as well, quenching the reaction at a specific reaction time. 
- P15 --> fig 1.2 source aanpassen en wat is agloit
	+ > source aangepast
	+ > agloit vervangen door exploit
- P17 --> Typfouten in bedrijfsnamen
	+ > aangepast
- P17 --> scaling strategien toelichten
	+ > Figuur caption is aangepast
- P17 --> eerder naar Roberge et al refereren en voorbeeld toevoegen van reactie types A, B en C
	+ > pagina eerder naar Roberge gerefereerd
	+ > toegevoegd in tabel
- P17 --> waar en wanneer is de capex study gedaan
	+ > US market 2014, tekst aangepast
- P17 --> zin vervolledigen
	+ > This shows that the movement of microreactors from academic research to industrial applications
	+ > vervangen door:
		* > This could indicate that the movement of microreactors from academic research to industrial applications is increasing.
- P18 --> figuur strategie toelichten
	+ > Caption aangepast
- P19 --> Bedrijfsnamen opkuisen
	+ > Opgekuisd
- P19 --> typfouten corrigeren
	+ > Aangepast
- P20 --> rephrasen
	+ > The microreactor can be thought of to be mixing in the microscale, which is confusing because it works in the mesoscale mixing level.
	+ > vervangen door:
		* >  The microreactor works in the mesoscale.
- P21 --> grafieken toelichten
	+ > caption aangepast
- P22 --> typfouten
	+ > aangepast
- P24 --> formule tussen grafiek en tekst eenduidig maken
	+ > aangepast
- P25 --> typfout
	+ > aangepast
- P26 --> typfout
	+ > aangepast
- P30 --> typfout
	+ > aangepast
- P31 --> "The use of acoustic waves induces cavitations" &lt;-- niet altijd
	+ > The use of acoustic waves can induces cavitations, which can lead to cavitational erosion, destroying the device in the long run.
- P32 --> 2de paragraaf opkuisen
	- \(t_m\) niet verwarren met mixing tijd voorgaand
		+ > vervangen door $t_{mat}$
	- Verduidelijk the shift van 4A naar SOD
		+ > vervangen door:
			* > An example of this is the transformation of 4A zeolite to sodalite in its respective hydrothermal environment during synthesis, described by \citet{Ding2010a}.
- P44 --> en US (ultrasound)
	+ > aangepast
- P45 --> CNC acroniem toelichten in tekst
	+ > in tekst toegevoegd
	- typfout
		+ > aangepast
	- the remaining techniques refereren naar tabel
		+ > aangepast
- P47 --> verduidelijken dat er enkel met CNC chips is gewerkt en waarom laserablatie niet is gebruikt
	+ > toegevoegd:
		* > After these key issues, the thesis diverted its attention towards CNC milling which was available at a low cost. This increased the idea-to-realization time and has a higher depth accuracy when partial cuts are required.
		* > All the reactors, evaluated throughput the remainder of the thesis, have been developed and produced using micro- milling. 
	- Visualisatie van chip load (figuur)
		+ > figuur toegevoegd
- P48 --> referentie en typfout
	+ > aangepast
	+ > (fabrication tolerance of supplier)
- P49 --> dimensies toevoegen aan figuur (misschien combineren met chip load?)
	+ > done
- P50 --> tabel beduidende cijfers en hoofdletter voor Datron
	+ > aangepast
- P52 --> 90/10 naar 90/10 %
	+ > aangepast
- P53 --> figuur 2.5 gebruiken in tekst.
	+ > referentie geplaatst in zin
- P56 --> waarom CNC in chapter 3-5 en laserablatie in 6
	+ > alles is met CNC gedaan wegens problemen met laser ablatie.
- P63 --> rephrase
	+ > As a third, these are the bifurcating flow distributors, which were studied are derived from nature
		* > As a third type are bifurcating flow distributors, which are derived from nature
- P65 --> Rephrase
	+ > A similar effect is at large when using flow focusing devices.
		* >  With increasing flow ratios (over 0.3), the droplet length / channel width will increase in an equal fashion. The shift from dripping to jetting is also present when using flow focusing devices 
- P66 --> typfouten
	+ > aangepast
- P67 --> rephrasen en typfouten
	+ > They showed that by reducing the flow crossection on each step down the 2/3rd of the previous crossection.
		* > In \ref{fig:design} a generic view is shown. This design was first proposed by \citet{Tondeur1998}. \citet{Luo2007a} extended the study both analytical and experimental for the optimal design regarding minimal holdup volume and pressure drop. They showed that by reducing the channel crossection each step to $2/3^{rd}$ of the previous crossection, the flow distribution will be uniform with minimal dressure drop increase \cite{Tondeur2004}.
- P68 --> ontbrekend werkwoord
	+ > zie vorige opmerking
	+ > aanpassingen aan tekst
- P70 --> 3 of 3.6 % opzoeken in data
	+ > 3.6 %
- P71 --> flow richting en kleurschaal toevoegen
	+ > togevoegd
	- Below a flow ratio of 0.1, an increase in droplet size and span is noticed
		- Dit opvangen door statistische studie op data en toevoegen	
			+ > Er is geen direct afwijking en de ivnloed van de geteste flow rate ratios en flow rates hebben geen rechtstreekse impact op het systeem. 
			+ > De tekst is aangepast
- P72 --> foutbar data toevoegen
	- Wat is net proportioneel met d^2
		+ > settling snelheid in laminair regime (Re < 2), tekst aangepast.
			* > Since the settling time in a laminar flow regime (Re $<$ 2) is proportional to $d^{-2}$ in phase separation, it is important to reduce the fraction of droplets smaller than the desired size.
	- Referentie toevoegen aan it is known
		+ > toegevoegd "E. L. Paul, V. a Atiemo-obeng, and S. M. Kresta, Handbook of Industrial Mixing: Science and Practics. 2004."
- P73 Figuren vervangen door link naar filmpjes voor vergelijk
	- it is clear --> tis niet duidelijk (op te vangen door quantitatieve data van scheiding? of referentie naar filmpjes toevoegen
		* > aangepast
	- typfout
- P74 --> dit valt niet uit de tekst te halen of is niet besproken
	+ >allowing for a droplet generation at 20-30 ml/min with a droplet size coefficient of variance less than 9 %, whereas less
		* > tekst is aangepast om hieraan tegemoet te komen
- P79 --> Chemical Engineering Journal
	+ > aangepast
- P80 --> two accounts ??
	+ > aangepast
		* > This correlation is fitted on two fronts (a and $k_la$)
- P81 --> result(s)
	+ > aangepast
- P90 --> niet volledig duidelijk, is het mogelijk om een voorbeeld te geven van performantie verschillen
	+ > sectie 4.2.2 geeft uitleg over performantie, er is een referentie toegevoegd om dit te verduidelijken.
		* > This is expressed as relative performance ($\alpha$, \ref{sec: FITC}) of the reactor to allow for a clear comparison between the combined extraction and mixing efficiency of each reactor type.
- P91 --> consistentie van grafiek assen
	+ > aangepast
- P92 --> figuur 4.6b toelichten van flow profiel van de ltf-mx
	+ > In contrast, the performance at very low energy dissipations is limited, which is attributed to the fact that no segmentation can be observed similar to the other reactors.
- P93 --> wat zou effectn zijn van andere flowrate ratios of allesinds toelichten in grafiek
	+ > De flow rate ratios zijn niet gewijzigd. De grootste impact bij een gelijke energie dissipatie ligt hem waarschijnlijk in het feit dat er een verschil van relatieve concentratie zal zijn en dus de drijvende kracht wijzigt.
	- is er effectief een groot verschil tussen 2.5 en 6 seconden verblijftijd
		+ > ja, de performantie vervalt tot $1/5^{de}$ en het verschil onderderling op gelijke tijdstippen is in extremis gelijk (> 6s) tot het dubbele (2.5 s)
- P94 --> typfout
	+ > aangepast
- P96 --> $C_6$ definieren als range constante
	+ > aangepast en toegelicht
	- theoretisch maximum en effectieve diameter toelichten, referentie naaar eq 4.8 en condities toevoegen van test
- P97 --> grafiek omzetten naar diameter ipv straal
	+ > aangepast
		* > zie comments Nico V
- P98 --> sensitivity analysis of the emperical fit??
	+ > In een plot weergegeven hieronder is een range van 10 % om de gefitte data 
	![fitting](C:/Users\tabias/OneDrive/PHDTHESIS/Rebuttal/Berekeningen/sensitivity.png)
	- is er een statistisch significant verschil
	- De studie is gekanteld naar energie dissipatie maar wat met settling (kan daar de zelfde veronderstelling gemaakt worden)
		+ > De studie is niet uitgevoerd op settling voor deze reactoren. Settling karakteristieken zijn meer uitgewerkt in chapter 5 maar er is geen direct correlatie gevonden tussen de verschillende aspecten.
- P107 --> Purification Technology
	+ > aangepast
- P111 --> gl -> g/l en Cyanex
	+ > aangepast
- P113 --> selectiviteit een eigen vergelijking
	+ > toegevoegd
- P115 --> Definitie van E en selectiviteit toevoegen
	+ > aangepast
- P116 --> hoeveel repetities
	+ > 3 of 5 repetities voor error bars
- P116 --> De selectiviteit van 23 op P117 slaat op?
	+ > typfout, moet 55 graden celcius zijn in tekst (niet 50) waar het een selectiviteit van 25 uitkomt (vortex mixer)
		* > tekst is aangepast
	- De grafiek bestaat enkel uit de vortex mixer (spheres op figuur 5.4)
		+ > caption aangepast
- P127 --> typfout
	+ > aangepast


- P134 --> discussie tussen kristallisatie snelheid en reactie snelheid toevoegen in paragraaf (of highlighten)
	+ > oplosbaarheid stijgt met toenemende temperatuur, kristal groei vertraagt
		* > De tekst is aangepast
- P135 --> temperatuur verhogen zorgt mss voor tragere groei versusr eactie met gevolg dat het mss beter is?
	+ > oplosbaarheid stijgt met toenemende temperatuur, kristal groei vertraagt. Dit kan leiden to betere kristal controle/zuiverheid.
		* > De tekst is aangepast
- P153 --> Zeer algemene conclusie
	+ > De conclusie is aangepast
	+ > Meer detailleren wat impact op SX nu juist is
	+ > Wat is impact op specifieke kristallisatie 
		* > TODO
	+ Welke kennisgaten moeten er nog effectief worden dichtgereden voor industriele toepassingen mogelijk zijn
		* > TODO



------------------------------------------------------------------------

Opmerkingen tijdens defense

---------------------------

- Slordige tekst, refs drama, statements te gratuit (zonder ref., staving, heel moeilijk om inhoud te begrijpen)
- Er ontbreken veel technische details: spreiding, procescondities, definities: kan thesis verbeteren
- Chemische reactoren in 3 verschillende types
- Fig. 3.7 geen verschil in gem. tussen 2 punten wegens grote foutenvlaggen? Variaties in totale flow rate: meer info nodig. Die resultaten moeten meer uitwerken
- Link naar youtube/website Fig. 3.8?
- Vgl sel (pag. 113): apart nummer geven
- CNC/ablatie: waarom afwisselend ablatie CNC, etc.: staat er fout in: allemaal met CNC
- Fig. 4.7: hoe significant zijn verschillen? Noticable shift between 2.5 s vs. 6 s? Toelichten
- Rare manier resultaten bespreken. Eerst zeggen: dit is wat ik zie, erna ev. verwijzen naar theorie (en niet omgekeerd).
- Bije elke Fig gebruikte symbolen opnieuw definiëren
- Uniformiseren: diameter versus r
- Fig. 6.5: waarom enkel bij hoge T kistallen? Verklaring? Discussie ook toespitsen op kristallisatie. Effect grootte? Kristalliniteit?
- Onderscheid maken tussen kristallisatie en reactie. Reden ook dat je bij hogere T kristallisatie trager. Zoeken in literatuur.
- 
------------------------------------------------------------------------

# Tom B

algemene opmerking --> taalfouten

## Opmerkingen tekst

- Scope --> typfouten oplossen
- P13 --> are we working on the first or third pillar in this work, or a combination of both?
	+ > De thesis gaat vooral over pillaren 1-3 maar beïnvloedt onrechtstreeks ook pillaar 4. Voor duidelijkheid is de zin toegevoegd;
		* > This work will be focused towards the first three pillars, intensifying the mixing and treatment conditions for chemical processes.
	+	baffle or blade ??
		*	> tekst is aangepast
	+	which <>
		*	> Alle "which" zijn gecontroleerd
- P14 --> figuur 1.1 E = fout (spreiding)
	+ > caption aangepast:
		* >The residence time distribution problem in plug flow (left) versus CSTR (right) with $\bar{t}$ the average residence time and the y-axis the outlet intensity.
- P15 --> explain decrease in citations/research
	+ > zie comments Nico
- P18 --> some sacrifices are made during scale-up, which ones?
	+	Example?
		*	> (eg. reduced mixing efficiency, lower heat transfer)
	+	does it all depend on the most critical ones? Explain
		*	> (eg. retaining heat transfer capacity whilst sacrificing mixing
efficiency)
- P19 --> what is bifurcation?
	+ > vertakking/splitsing
	+	Kolmogorov scale? dieper uitleggen
		*	> $\eta = \nu^{3/4} \varepsilon^{-1/4}$ is the Kolmogorov length scale induced by turbulent movement and is assumed the smallest possible length scale achievable by means of mixing.

----------------

- P21 --> fig 1.5 it is not clear for me what is in the Y-axis, also in the caption (a), (b)
	+ > caption aangepast
		* > (a)} The relation of the mixing time and the energy dissipation (Epsilon, $\varepsilon$) and \textbf{(b)} Re/Pe with mixing time ($t_m/d^2$ with $t_m$ the mixing time and $d$ the characterstic reactor dimension) in single phase mixing for several mixers

- P25 --> Correlatie We met $\varepsilon$
	+	> power number toegevoegd
- P44 --> Did you consider 3D printing? Why not, what are the drawbacks (list it)
	+ > we sure did, but the inter-layer roughness is within the range of 20-30 µm making it prone for fouling.
- P45 --> waarom dan wel lithografie in tekst?
	+ > lithografie chips zijn effectief gemaakt maar voldeden niet aan de vereisten.
- P46 --> fig 2.1 explain the evaluation of laser ablation and CNC milling (clarify chapter to distinguish)
	+ > het hoofdstuk is bewerkt om het onderscheidt te maken en te verduidelijken dat de rest van de thesis zich focust op gefreesde chips.
		* > After these key issues, the thesis diverted its attention towards CNC milling which was available at a low cost. This increased the idea-to-realization time and has a higher depth accuracy when partial cuts are required.
- P47 --> discrepantie in tekst --> uniform trekken en duidelijk maken (zie vorige comment)
	+ > zie vorige comment
- P50 --> wat is accuracy van frezen?
	+ > The supplied tools have a diameter offset of 0.03 $\mu m$ or 1 \% [specifications from supplier] but were always assumed to be the exact specification. The accuracy of the stepper motors in x and y direction was 0.5 $\mu m$. 
- P49 --> What about accuracy? (this is in respect to laser ablation --> the reason it is omitted)
	+ > het probleem met laser ablatie is de tapering van de wand en de diepete (z) bij deel-snedes (niet volledig door het substraat). Dit heeft na analyse geleidt tot het volledig overgaan naar CNC milling
- P52 --> new tool 
	+ > vervangen door fresh tool
	+	DCM methode beter toelichten (met illustratie?)
- P53 --> with some smart design ...
	+ > deleted
- P55 --> skilled operator ...
	+ > deleted
	+	through cuts <> partial cuts ??
		*	> door oppervlak of een deelse snede
- P63 --> what are the pitfalls of numbering up?
	+	Is flow distribution easy?
		*	> Afhankelijk van de schaal en de noodzaak aan minimaal volume kan dit zeer moeilijk zijn.
		*	> De grootste zorg bij numbering up zonder vloeistofverdeler/collector is de noodzaak aan nutsvoorzieningen.
		*	> Dit is ook specifieker toegelicht in de thesis
- P64 --> illustratie toevoegen van 2D en 3D distributoren
	+	wie is "one"
		*	> Vervangen door auteur
	+	lumb factor?
		*	> veranderd
	+	Ca nummer toelichten
- P65 --> fig 3.1 what is the slope 1:1 (clarify)
	+ > De slope 1:1 geeft aan dat de druppel-lengte/kanaal-breedte verhouding bij een hogere ratio $Q_d/Q_c$ zich evenredig gedraagt als deze ratio
	+	What is the theoretical idea behind this (dripping -> squeezing -> jetting?)
		*	> squeezing
			-	> De druk gradient in het volledige kanaal ten gevolge van uitkomende discontinue fase zorgt ervoor dat de visceuze krachten in de discontinue fase overwonnen worden en er een druppel af gesplitst wordt.
		*	> dripping
			-	> De discontinue fase komt in een partieel parallele lijn met de continue fase maar splitst af door overwonnen schuifkrachten
		*	> jetting
			-	> gelijkaardig aan dripping maar aan hogere snelheid en er vormt zich een Rayleigh-Taylor instabiliteit waardoor er een variatie aan druppelgroottes wordt afgesplitst.
- P68 --> figuur 3.3 toelichten dat het duidelijk is dat uitgangen uitgangen zijn
	+ te lange zin
		* > aangepast
- P69 --> fig 3.4 beter toelichten
	+ > aangepast
- P71 --> describe fig 3.7(b), equal total flow rate <-> fig 3.7(b).
	+	Increase in droplet size (not apparent, statistical analysis)
	+	> zie comments aan Tom VG
- P70 --> fig 3.5 variance 3 <-> 3.6 %
	+ 	> 3.6 %
- P72 --> fig 3.8 phase separation not clear (movie)
	+ > omgezet naar video
- P87 --> describe the 4 zones (enhance figures and measuring zones)
	+ > beschrijvende figuur toegevoegd
- P90 --> why pH graph and high fluorescence @ 0.85 W/O ratio
	+ > fluorescente activiteit van FITC valt weg bij hoge pH. De pKa van FITC is 5.2.
	+	non-linear behavior continues below 10 ml/min
		*	> Dit is normaal gedrag, de tekst is aangepast
- P92 --> verklaar fig 4.6 nader
- P93 --> verklaar waarom LTF-MX minder is op lage flowrate (stratified)
- P94 --> fig 4.7 noticeable shift from 2.5 to 6 seconds (explain)
	+ > De shift van 2.5 naar 6 seconden of de stappen ertussen geven een verval van de $k_la$ 2 tot 4x aan wat an-sich significant is. Het onderscheidt is hier het grootste. 
	+ > Deze grafiek en conclusie zit er tevens in om de conclusies uit gelijkaardige studies te nuanceren en de noodzaak van de correlatie met energie dissipatie te definieren, eerder dan met $\tau$.
- P97 --> N_sc toelichten
	+ > vergelijking toegevoegd
	+	radius < > diameter
		*	> tekst uniform getrokken
- P99 --> eq 4.8 explain c6
	+ > emperische fit constante (range 0.53-0.6, tekst aangepast)
	+ > zie uitgebreidere comments bij Nico V
- P109 --> wat is ondergrens van druppelgrootte waar ze nog nuttig zijn (kolmogorov scale helps)
	+ > De minimale druppelgrootte is sterk afhankelijk van de processen die nodig zijn. 
	+ > De optimale druppelgrootte is deze waar de massa-transfer limiet opgeheven wordt indien dit de limiterende snelheid is. Indien dit niet het geval is of  de fasescheiding is de snelheidsbepalende stap, dan dient de druppelgrootte toe te nemen tot dit in evenwicht is. (debottlenecking)
- P116 --> fig 5.4b differentiate between LTF-MX and vortex mixer (this is vortex mixer)
	+ > aangepast
- P119 --> fig 5.7 in log scale?
	+ > De resultaten om de rand zijn zijn 99 % extractie
		* > figuur aangepast naar log schaal
- P129 --> what is NaA and what is a linde type A topology (inform)
	+ > Illustratieve figuur van LTA toegevoegd
	+	how to deal with RTD in CSTR
		*	> In chapter 1 is het probleem van RTD in CSTR's toegelicht. 
		*	> Mogelijke oplossingen zijn effectief overschakelen naar plug flow reactoren of het geluk hebben dan het chemische proces naar een evenwicht gaat zonder nevenproducten.
		*	> Indien bovenstaande argument niet geldt, dan dient er opgezuiverd te worden.
- P134 --> what is difference between stirred and unstirred batch heating (include information)
	+ > De maximale temperatuur waarmee de geroerde batch kan stijgen is de maximale overdracht van de autoclaaf naar de vloeistof wat zich uit in een benodigde tijd van 100 min.
		* >  The use of a stirred autoclave would improve the temperature profile, but it would still be limited by the thermal conductivity of the PTFE liner and SS casing.
- P135 --> tolerance barrier in fig 6.4(a) but not in (b) (include it)
	+ > toegevoegd
- P136 --> you mean the comparison with a batch reactor AFTER 2 hours (yes, need to reach temp)
	+ > Ja, anders is de reactor nooit op temperatuur is
- P137 --> you mention no crystalline material in the CFR for 16 min at 100 C, explain this)
	+ > De XRD resultaten tonen enkel amorf materiaal.
- Pxx --> fig 6.9 is not clear (caption and legend)
	+ > uitleg in tekst toegevoegd.
		* > Besides the composition optimization, the influence of temperature can be depicted. With increasing temperature, the batch synthesized 4A zeolite quantity decreases significantly due to a conversion to SOD. In the CFR, the synthesis at limited residence time increases with temperature but reaches similar adsorption results as a batch synthesized 4A (4 hours) in only 16 min.
- Pxx --> hoe betrouwbaar is reproductie data, vooral duidend op hfdst. 5
	+ > De resultaten van herhaalde testen op dezelfde startmaterialen wijken maximale afwijking is 3 % van de gemiddelde.
- Pxx --> numbering up in future perspectives?

## Opmerkingen discussie

- Procesintensificatie. In welke pijler? Toelichten! Combinatie 1 en 3
	+ > De pijlers zijn toegelicht, telkens met een sprekend voorbeeld en een paragraaf is toegevoegd om het werk te situeren
- Fig. 1.1 Wat is E?
	+ > verblijftijdspreiding, tekst aangepast
- Fig. 1.5 y as legende onduidelijk
	+ > verduidelijkt
- Pag. 25: laatste zin: vanwaar komt dit? Power number?
	+ > power number beschrijving toegevoegd
- Limitaties numbering up?
	+ > De limitaties van numbering up of de kostfactor is toegevoegd aan het werk
- Pag 70 3.6 % en 3% (Fig. 3.6)
	+ > 3.6 %
- Fig. 4.1 andere observation points: zal te zien zijn bij kleur versie

	+ > De figuren zijn aangepast en verduidelijkt zodanig dat het op zwart-wit print ook duidelijk is
- Pag. 109: the only wa y is by decreasing the size: optimum? Kort door de bocht statement
	+ > De statement aangepast:
- Pag. 129 Zeoliet Linde type A topologie? Zijn er ook andere topologieën?
	+ > Ja er zijn zeker andere topologien IZA( international zeolite association - KUL) heeft hier een duidelijke beschrijving van (zie ref)
	+ > ref:referentie naar iza
	+ > De tekst is verduidelijkt en de link is ingevoegd om de topologie te plaatsen
- Fig. 4.4 wat is stippellijn?
	+ > baseline
- Fig. 4.4 Vanaf 10 ml niet lineair. Eronder wel? Statement aanpassen: is gewoon normaal gedrag.
	+ > De statement is aangepast naar volgende paragraaf:
- Afwijkingen door milling?
	+ > De afwijking van milling is 3-4 µm in xy-richting op de instelling
	+ > Deze afwijking bij goede instellingen is reproduceerbaar en heeft weinig impact op de totale flow karakteristieken aangezien de kanaal dimensies typisch > 1 mm zijn.
- Mogelijke fabricagetechnieken. Zou je ook andere technieken gebruiken als geld geen issue zou zijn.
	+ > LIGA, injection molding voor massa productie?
- Fig. 6.4b: tolerance boundary: waarom niet bij Fig. 6.4a
	+ > De tolerantie boundaries zijn ook toegevoegd aan fig 6.4a

- Fig. 6.9: waarom afwijkend punt: uitleggen, driehoek toevoegen zou nuttig zijn
	+ > 3hoek is toegevoegd

------------------------------

# Johan DC

## Opmerkingen gesprek

- Druppelproductie: Vergelijk met conventionele methode: E nodig per debiet
- Wat is onderliggende reden 4 pillaren? Entropie? Alles wat niet gelijk is en niet even snel reageert vermindert de efficiëntie.
- Hoe ziet u verdere valorisatie?
	- Voor umicore: partikels
	- Farma: druppels, alleen aan grote, of ook in grote aantallen, hoe goedkoop te maken, lijn doortrekken naar praktijk
- Aspect praktisch vervolg: uit de doeken doen in thesis
	- Valorisatie
	- Kost: investering voor bedrijf
- Belangrijke conclusies duidelijk geven in tekst, ook wat over valorisatie
- Wijzigingen in track changes

-----------------------------

# Heidi O

- Welke image analysis? Nauwkeurigheid? Afschatting fout? Contrast te verbeteren bij Fig. 4.9
- Waarom voor PMMA gekozen?

-----------------------------------

# Kris D

- Trend academic output? Specifiek (gebrek aan literatuur bij) hydrometallurgie toelichten
- Fig. 4.9: waarom bimodaal?
- Wat voor mixers gebruiken bij slurries? Mixers daar ook voor geschikt?
