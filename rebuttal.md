Ken B.

-   Refs

-   Symbolen alfabetisch

-   Capillary number: def geven en fysische betekenis

-   Hoe kom je tot vgl. 4.8

Nico V.

-   nergens definitie mesoflow reactor

-   wat is reden afname publicaties?

-   LTF, vortex, andere: waarom net die 3 reactoren?

<!-- -->

-   Pag. 16: welke reactie is bvb. exotherm? Ammoniak, gevaarlijke
    zaken: voorbeelden geven van verschillende types reacties

-   Kolmogorov scale? Wat? Geen antwoord.

-   Pag 26: hoe kan je oversaturatie nog bereiken: koeling, anti-solvent
    kristallisatie: deze 2 types ook toevoegen

-   H3: nood aan verduidelijkende Fign: Desmet design, bifurcating, etc.

-   Pag. 64?

-   Fig. 3.1 the main drawback… klopt dit, increase capillary number kna
    je niet halen uit Fig. 3.1

-   P. 66: 2.5 D, 3D: Fig zou goed zijn

-   Waarom SDS toegevoegd? Truukje, maar ook voor batch gebruikt (ook
    bij Tom VG)

-   Meer uitleg over simulaties: solvent settings (experimental hiervan)

-   Fig. 3.7: hoe droplet size gemeten?

-   Soort PSD curve toevoegen (distributie)?pag. 72: ‘droplet
    distribution is large’: van waar? Uitleg?

-   Pag. 73?

-   P80: onduidelijk

-   Fig. 4.5: op welke basis punten gekozen? Op basis van afstand?
    Eerlijk? Beter gelijke verblijfstijd? Nee. Vergelijking fair?

-   Fig. 4.6 energie-input vergelijken met convent. Reactor: opzoeken in
    literatuur: grootte-ordes geven

-   Selectiviteit beter of slechter met geteste reactoren? Hoe ver zit
    je van theor. max? Schalen op max?

-   Fig. 4.9: kleine druppels ‘optisch’ weggefilter? Tobias: het is wel
    zo dat gebruikte theor. waarde op basis van

-   Fig. 4.9 , betere keuze mogelijk dan 0.6

-   Theor berekening 8 µm verschil met theorie: verhaal gaat niet op: 2
    versch zaken? Waarom klopt theorie niet met exp?

-   Alternatieve manier om watercontent te bepalen? Karl Fischer.

Tom VG

-   Slordige tekst, refs drama, statements te gratuit (zonder ref.,
    staving, heel moeilijk om inhoud te begrijpen)

-   Er ontbreken veel technische details: spreiding, procescondities,
    definities: kan thesis verbeteren

-   Chemische reactoren in 3 verschillende types

-   Fig. 3.7 geen verschil in gem. tussen 2 punten wegens grote
    foutenvlaggen? Variaties in totale flow rate: meer info nodig. Die
    resultaten moeten meer uitwerken

-   Link naar youtube/website Fig. 3.8?

-   Vgl sel (pag. 113): apart nummer geven

-   CNC/ablatie: waarom afwisselend ablatie CNC, etc.: staat er fout in:
    allemaal met CNC

-   Fig. 4.6: waarom gaat xx curve: , 4.6 B niet duidelijk

-   Fig. 4.7: hoe significant zijn verschillen? Noticable shift between
    2.5 s vs. 6 s? Toelichten

-   Rare manier resultaten bespreken. Eerst zeggen: dit is wat ik zie,
    erna ev. verwijzen naar theorie (en niet omgekeerd).

-   Bije elke Fig gebruikte symbolen opnieuw definiëren

-   Uniformiseren: diameter versus r

-   Fig. 6.5: waarom enkel bij hoge T kistallen? Verklaring? Discussie
    ook toespitsen op kristallisatie. Effect grootte? Kristalliniteit?

    Onderscheid maken tussen kristallisatie en reactie. Reden ook dat je
    bij hogere T kristallisatie trager. Zoeken in literatuur.

Tom B

-   Procesintensificatie. In welke pijler? Toelichten! Combinatie 1 en 3

-   Fig. 1.1 Wat is E?

-   Fig. 1.5 y as legende onduidelijk

-   Pag. 25: laatste zin: vanwaar komt dit? Power number?

-   Limitaties numbering up?

-   Pag 70 3.6 % en 3% (Fig. 3.6)

-   Fig. 4.1 andere observation points: zal te zien zijn bij kleur
    versie

-   Pag. 109: the only wa y is by decreasing the size: optimum? Kort
    door de bocht statement

-   Pag. 129 Zeoliet Linde type A topologie? Zijn er ook andere
    topologieën?

-   Fig. 4.4 wat is stippellijn?

-   Fig. 4.4 Vanaf 10 ml niet lineair. Eronder wel? Statement aanpassen:
    is gewoon normaal gedrag.

-   Afwijkingen door milling?

-   Mogelijke fabricagetechnieken. Zou je ook andere technieken
    gebruiken als geld geen issue zou zijn.

-   Fig. 6.4b: tolerance boundary: woorom niet bij Fig. 6.4 a

-   Fig. 6.9: waarom afwijkend punt: uitleggen, driehoek toevoegen zou
    nuttig zijn

Johan DC

-   Druppelproductie: Vergelijk met conventionele methode: E nodig per
    debiet

-   Wat is onderliggende reden 4 pillaren? Entropie? Alles wat niet
    gelijk is en niet even snel reageert vermindert de efficiëntie.

-   Hoe ziet u verdere valorisatie?

    -   Voor umicore: partikels

    -   Farma: druppels, alleen aan grote, of ook in grote aantallen,
        hoe goedkoop te maken, lijn doortrekken naar praktijk

-   Aspect praktisch vervolg: uit de doeken doen in thesis

    -   Valorisatie

    -   Kost: investering voor bedrijf

-   Belangrijke conclusies duidelijk geven in tekst, ook wat over
    valorisatie

-   Wijzigingen in track changes

Heidi O

-   Welke image analysis? Nauwkeurigheid? Afschatting fout? Contrast te
    verbeteren bij Fig. 4.9

-   Waarom voor PMMA gekozen?

Kris D

-   Trend academic output? Specifiek (gebrek aan literatuur bij)
    hydrometallurgie toelichten

-   Fig. 4.9: waarom bimodaal?

-   Wat voor mixers gebruiken bij slurries? Mixers daar ook voor
    geschikt?


